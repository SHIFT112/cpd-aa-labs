
helloflops1vec_xphi:     file format elf64-little


Disassembly of section .init:

0000000000400510 <_init>:
  400510:	48                   	dec    %eax
  400511:	83 ec 08             	sub    $0x8,%esp
  400514:	e8 b3 00 00 00       	call   4005cc <call_gmon_start>
  400519:	e8 12 01 00 00       	call   400630 <frame_dummy>
  40051e:	e8 b1 07 00 00       	call   400cd4 <__do_global_ctors_aux>
  400523:	48                   	dec    %eax
  400524:	83 c4 08             	add    $0x8,%esp
  400527:	c3                   	ret    

Disassembly of section .plt:

0000000000400530 <.plt>:
  400530:	ff 35 82 0e 20 00    	pushl  0x200e82
  400536:	ff 25 84 0e 20 00    	jmp    *0x200e84
  40053c:	0f 1f 40 00          	nopl   0x0(%eax)
  400540:	ff 25 82 0e 20 00    	jmp    *0x200e82
  400546:	68 00 00 00 00       	push   $0x0
  40054b:	e9 e0 ff ff ff       	jmp    400530 <_init+0x20>
  400550:	ff 25 7a 0e 20 00    	jmp    *0x200e7a
  400556:	68 01 00 00 00       	push   $0x1
  40055b:	e9 d0 ff ff ff       	jmp    400530 <_init+0x20>
  400560:	ff 25 72 0e 20 00    	jmp    *0x200e72
  400566:	68 02 00 00 00       	push   $0x2
  40056b:	e9 c0 ff ff ff       	jmp    400530 <_init+0x20>
  400570:	ff 25 6a 0e 20 00    	jmp    *0x200e6a
  400576:	68 03 00 00 00       	push   $0x3
  40057b:	e9 b0 ff ff ff       	jmp    400530 <_init+0x20>
  400580:	ff 25 62 0e 20 00    	jmp    *0x200e62
  400586:	68 04 00 00 00       	push   $0x4
  40058b:	e9 a0 ff ff ff       	jmp    400530 <_init+0x20>
  400590:	ff 25 5a 0e 20 00    	jmp    *0x200e5a
  400596:	68 05 00 00 00       	push   $0x5
  40059b:	e9 90 ff ff ff       	jmp    400530 <_init+0x20>

Disassembly of section .text:

00000000004005a0 <_start>:
  4005a0:	31 ed                	xor    %ebp,%ebp
  4005a2:	49                   	dec    %ecx
  4005a3:	89 d1                	mov    %edx,%ecx
  4005a5:	5e                   	pop    %esi
  4005a6:	48                   	dec    %eax
  4005a7:	89 e2                	mov    %esp,%edx
  4005a9:	48                   	dec    %eax
  4005aa:	83 e4 f0             	and    $0xfffffff0,%esp
  4005ad:	50                   	push   %eax
  4005ae:	54                   	push   %esp
  4005af:	49                   	dec    %ecx
  4005b0:	c7 c0 d0 0c 40 00    	mov    $0x400cd0,%eax
  4005b6:	48                   	dec    %eax
  4005b7:	c7 c1 70 0c 40 00    	mov    $0x400c70,%ecx
  4005bd:	48                   	dec    %eax
  4005be:	c7 c7 60 06 40 00    	mov    $0x400660,%edi
  4005c4:	e8 a7 ff ff ff       	call   400570 <_init+0x60>
  4005c9:	f4                   	hlt    
  4005ca:	66 90                	xchg   %ax,%ax

00000000004005cc <call_gmon_start>:
  4005cc:	48                   	dec    %eax
  4005cd:	83 ec 08             	sub    $0x8,%esp
  4005d0:	48                   	dec    %eax
  4005d1:	8b 05 d1 0d 20 00    	mov    0x200dd1,%eax
  4005d7:	48                   	dec    %eax
  4005d8:	85 c0                	test   %eax,%eax
  4005da:	74 02                	je     4005de <call_gmon_start+0x12>
  4005dc:	ff d0                	call   *%eax
  4005de:	48                   	dec    %eax
  4005df:	83 c4 08             	add    $0x8,%esp
  4005e2:	c3                   	ret    
  4005e3:	90                   	nop

00000000004005e4 <__do_global_dtors_aux>:
  4005e4:	55                   	push   %ebp
  4005e5:	48                   	dec    %eax
  4005e6:	89 e5                	mov    %esp,%ebp
  4005e8:	53                   	push   %ebx
  4005e9:	52                   	push   %edx
  4005ea:	80 3d 8f 0e 20 00 00 	cmpb   $0x0,0x200e8f
  4005f1:	75 39                	jne    40062c <__do_global_dtors_aux+0x48>
  4005f3:	b8 b8 11 60 00       	mov    $0x6011b8,%eax
  4005f8:	48                   	dec    %eax
  4005f9:	2d b0 11 60 00       	sub    $0x6011b0,%eax
  4005fe:	48                   	dec    %eax
  4005ff:	c1 f8 03             	sar    $0x3,%eax
  400602:	48                   	dec    %eax
  400603:	8d 58 ff             	lea    -0x1(%eax),%ebx
  400606:	eb 11                	jmp    400619 <__do_global_dtors_aux+0x35>
  400608:	48                   	dec    %eax
  400609:	ff c2                	inc    %edx
  40060b:	48                   	dec    %eax
  40060c:	89 15 76 0e 20 00    	mov    %edx,0x200e76
  400612:	ff 14 d5 b0 11 60 00 	call   *0x6011b0(,%edx,8)
  400619:	48                   	dec    %eax
  40061a:	8b 15 68 0e 20 00    	mov    0x200e68,%edx
  400620:	48                   	dec    %eax
  400621:	39 da                	cmp    %ebx,%edx
  400623:	72 e3                	jb     400608 <__do_global_dtors_aux+0x24>
  400625:	c6 05 54 0e 20 00 01 	movb   $0x1,0x200e54
  40062c:	58                   	pop    %eax
  40062d:	5b                   	pop    %ebx
  40062e:	5d                   	pop    %ebp
  40062f:	c3                   	ret    

0000000000400630 <frame_dummy>:
  400630:	55                   	push   %ebp
  400631:	48                   	dec    %eax
  400632:	83 3d 87 0b 20 00 00 	cmpl   $0x0,0x200b87
  400639:	48                   	dec    %eax
  40063a:	89 e5                	mov    %esp,%ebp
  40063c:	74 12                	je     400650 <frame_dummy+0x20>
  40063e:	b8 00 00 00 00       	mov    $0x0,%eax
  400643:	48                   	dec    %eax
  400644:	85 c0                	test   %eax,%eax
  400646:	74 08                	je     400650 <frame_dummy+0x20>
  400648:	5d                   	pop    %ebp
  400649:	bf c0 11 60 00       	mov    $0x6011c0,%edi
  40064e:	ff e0                	jmp    *%eax
  400650:	5d                   	pop    %ebp
  400651:	c3                   	ret    
  400652:	66 90                	xchg   %ax,%ax
  400654:	66 90                	xchg   %ax,%ax
  400656:	66 90                	xchg   %ax,%ax
  400658:	66 90                	xchg   %ax,%ax
  40065a:	66 90                	xchg   %ax,%ax
  40065c:	66 90                	xchg   %ax,%ax
  40065e:	66 90                	xchg   %ax,%ax

0000000000400660 <main>:

//
// Main program - pedal to the metal...calculate tons o'flops!
// 
int main(int argc, char *argv[] ) 
{
  400660:	55                   	push   %ebp
  400661:	48                   	dec    %eax
  400662:	89 e5                	mov    %esp,%ebp
  400664:	48                   	dec    %eax
  400665:	83 e4 80             	and    $0xffffff80,%esp
  400668:	48                   	dec    %eax
  400669:	81 ec 80 00 00 00    	sub    $0x80,%esp
  40066f:	6a 03                	push   $0x3
  400671:	5f                   	pop    %edi
  400672:	e8 19 05 00 00       	call   400b90 <__intel_new_proc_init_R>
  400677:	0f ae 1c 24          	stmxcsr (%esp)
  40067b:	bf 08 14 60 00       	mov    $0x601408,%edi
  400680:	33 f6                	xor    %esi,%esi
  400682:	81 0c 24 40 80 00 00 	orl    $0x8040,(%esp)
  400689:	33 c0                	xor    %eax,%eax
  40068b:	0f ae 14 24          	ldmxcsr (%esp)
  40068f:	e8 ac fe ff ff       	call   400540 <_init+0x30>
    
    //
    // initialize the compute arrays 
    //

    printf("Initializing\r\n");
  400694:	bf 44 0d 40 00       	mov    $0x400d44,%edi
  400699:	33 c0                	xor    %eax,%eax
  40069b:	e8 b0 fe ff ff       	call   400550 <_init+0x40>
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
        fb[i] = (float)i + 0.2;
  4006a0:	ba ff 00 00 00       	mov    $0xff,%edx
    //
    // initialize the compute arrays 
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
  4006a5:	33 c0                	xor    %eax,%eax
    {
    fa[i] = (float)i + 0.1;
        fb[i] = (float)i + 0.2;
  4006a7:	c5 f8 92             	(bad)  
  4006aa:	d2 ba 00 ff 00 00    	sarb   %cl,0xff00(%edx)
    //
    // initialize the compute arrays 
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
  4006b0:	62                   	(bad)  
  4006b1:	f2 79 08             	repnz jns 4006bc <main+0x5c>
  4006b4:	58                   	pop    %eax
  4006b5:	25 8e 07 00 00       	and    $0x78e,%eax
  4006ba:	62                   	(bad)  
  4006bb:	f1                   	icebp  
  4006bc:	78 08                	js     4006c6 <main+0x66>
  4006be:	28 1d bc 06 00 00    	sub    %bl,0x6bc
    {
    fa[i] = (float)i + 0.1;
  4006c4:	62                   	(bad)  
  4006c5:	f2 f9                	repnz stc 
  4006c7:	08 19                	or     %bl,(%ecx)
  4006c9:	15 42 07 00 00       	adc    $0x742,%eax
  4006ce:	62                   	(bad)  
  4006cf:	f1                   	icebp  
  4006d0:	71 08                	jno    4006da <main+0x7a>
  4006d2:	ef                   	out    %eax,(%dx)
  4006d3:	c9                   	leave  
        fb[i] = (float)i + 0.2;
  4006d4:	c5 f8 92             	(bad)  
  4006d7:	ca 62 f2             	lret   $0xf262
  4006da:	f9                   	stc    
  4006db:	08 19                	or     %bl,(%ecx)
  4006dd:	05 36 07 00 00       	add    $0x736,%eax
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
  4006e2:	62                   	(bad)  
  4006e3:	f3 78 08             	repz js 4006ee <main+0x8e>
  4006e6:	cb                   	lret   
  4006e7:	eb 00                	jmp    4006e9 <main+0x89>
        fb[i] = (float)i + 0.2;
  4006e9:	62                   	(bad)  
  4006ea:	f1                   	icebp  
  4006eb:	61                   	popa   
  4006ec:	08 fe                	or     %bh,%dh
  4006ee:	dc 62 f3             	fsubl  -0xd(%edx)
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
  4006f1:	79 08                	jns    4006fb <main+0x9b>
  4006f3:	07                   	pop    %es
  4006f4:	f5                   	cmc    
  4006f5:	ee                   	out    %al,(%dx)
  4006f6:	62 71 78             	bound  %esi,0x78(%ecx)
  4006f9:	08 5a e5             	or     %bl,-0x1b(%edx)
  4006fc:	62 71 78             	bound  %esi,0x78(%ecx)
  4006ff:	08 5a ee             	or     %bl,-0x12(%edx)
  400702:	62                   	(bad)  
  400703:	d1 e9                	shr    %ecx
  400705:	08 58 fc             	or     %bl,-0x4(%eax)
  400708:	62 51 e9             	bound  %edx,-0x17(%ecx)
  40070b:	08 58 cd             	or     %bl,-0x33(%eax)
        fb[i] = (float)i + 0.2;
  40070e:	62                   	(bad)  
  40070f:	c1 f9 08             	sar    $0x8,%ecx
  400712:	58                   	pop    %eax
  400713:	c5 62 71             	lds    0x71(%edx),%esp
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
  400716:	f9                   	stc    
  400717:	88 5a c7             	mov    %bl,-0x39(%edx)
        fb[i] = (float)i + 0.2;
  40071a:	62 51 f9             	bound  %edx,-0x7(%ecx)
  40071d:	08 58 f4             	or     %bl,-0xc(%eax)
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
  400720:	62 51 f9             	bound  %edx,-0x7(%ecx)
  400723:	88 5a d1             	mov    %bl,-0x2f(%edx)
        fb[i] = (float)i + 0.2;
  400726:	62 a1 f9 88 5a c8    	bound  %esp,-0x37a57707(%ecx)
  40072c:	62 51 f9             	bound  %edx,-0x7(%ecx)
  40072f:	88 5a fe             	mov    %bl,-0x2(%edx)
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
  400732:	62 52 71             	bound  %edx,0x71(%edx)
  400735:	0a 65 d8             	or     -0x28(%ebp),%ah
        fb[i] = (float)i + 0.2;
  400738:	62                   	(bad)  
  400739:	c2 71 0a             	ret    $0xa71
  40073c:	65 d7                	xlat   %gs:(%ebx)
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
  40073e:	62 53 79             	bound  %edx,0x79(%ebx)
  400741:	09 07                	or     %eax,(%edi)
  400743:	da 44 62 a3          	fiaddl -0x5d(%edx,%eiz,2)
        fb[i] = (float)i + 0.2;
  400747:	79 09                	jns    400752 <main+0xf2>
  400749:	07                   	pop    %es
  40074a:	d1 44 62 71          	roll   0x71(%edx,%eiz,2)
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
    fa[i] = (float)i + 0.1;
  40074e:	7b 88                	jnp    4006d8 <main+0x78>
  400750:	29 1c 85 c0 14 a0 00 	sub    %ebx,0xa014c0(,%eax,4)
  400757:	c5 fa ae             	(bad)  
  40075a:	3c 85                	cmp    $0x85,%al
  40075c:	c0 14 a0 00          	rclb   $0x0,(%eax,%eiz,4)
        fb[i] = (float)i + 0.2;
  400760:	62                   	(bad)  
  400761:	e1 7b                	loope  4007de <main+0x17e>
  400763:	88 29                	mov    %ch,(%ecx)
  400765:	14 85                	adc    $0x85,%al
  400767:	c0 14 60 00          	rclb   $0x0,(%eax,%eiz,2)
  40076b:	c5 fa ae             	(bad)  
  40076e:	3c 85                	cmp    $0x85,%al
  400770:	c0 14 60 00          	rclb   $0x0,(%eax,%eiz,2)
    //
    // initialize the compute arrays 
    //

    printf("Initializing\r\n");
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
  400774:	48                   	dec    %eax
  400775:	83 c0 10             	add    $0x10,%eax
  400778:	48                   	dec    %eax
  400779:	3d 00 00 10 00       	cmp    $0x100000,%eax
  40077e:	0f 82 5e ff ff ff    	jb     4006e2 <main+0x82>
  400784:	f0 83 04 24 00       	lock addl $0x0,(%esp)
    {
    fa[i] = (float)i + 0.1;
        fb[i] = (float)i + 0.2;
    }	

    printf("Starting Compute\r\n");
  400789:	bf 54 0d 40 00       	mov    $0x400d54,%edi
  40078e:	33 c0                	xor    %eax,%eax
  400790:	e8 bb fd ff ff       	call   400550 <_init+0x40>
//
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
  400795:	48                   	dec    %eax
  400796:	8d 7c 24 18          	lea    0x18(%esp),%edi
  40079a:	33 f6                	xor    %esi,%esi
  40079c:	e8 ef fd ff ff       	call   400590 <_init+0x80>
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
  4007a1:	ba 01 00 00 00       	mov    $0x1,%edx

    printf("Starting Compute\r\n");

    tstart = dtime();	
    // loop many times to really get lots of calculations
    for(j=0; j<MAXFLOPS_ITERS; j++)  
  4007a6:	33 c0                	xor    %eax,%eax
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
  4007a8:	c5 f8 92             	(bad)  
  4007ab:	ca 62 e1             	lret   $0xe162
  4007ae:	7a 19                	jp     4007c9 <main+0x169>
  4007b0:	7a 4c                	jp     4007fe <main+0x19e>
  4007b2:	24 08                	and    $0x8,%al
  4007b4:	62                   	(bad)  
  4007b5:	f1                   	icebp  
  4007b6:	7a 19                	jp     4007d1 <main+0x171>
  4007b8:	e6 4c                	out    %al,$0x4c
  4007ba:	24 09                	and    $0x9,%al
  4007bc:	62                   	(bad)  
  4007bd:	f2 f1                	repnz icebp 
  4007bf:	11 98 0d 6a 06 00    	adc    %ebx,0x66a0d(%eax)
  4007c5:	00 62 f1             	add    %ah,-0xf(%edx)
  4007c8:	7a 19                	jp     4007e3 <main+0x183>
  4007ca:	7a 44                	jp     400810 <main+0x1b0>
  4007cc:	24 06                	and    $0x6,%al
  4007ce:	62                   	(bad)  
  4007cf:	e1 7a                	loope  40084b <main+0x1eb>
  4007d1:	19 e6                	sbb    %esp,%esi
  4007d3:	4c                   	dec    %esp
  4007d4:	24 07                	and    $0x7,%al
  4007d6:	62                   	(bad)  
  4007d7:	e2 f9                	loop   4007d2 <main+0x172>
  4007d9:	19 98 0d 50 06 00    	sbb    %ebx,0x6500d(%eax)
  4007df:	00 62 e2             	add    %ah,-0x1e(%edx)
        fb[i] = (float)i + 0.2;
    }	

    printf("Starting Compute\r\n");

    tstart = dtime();	
  4007e2:	f1                   	icebp  
  4007e3:	19 b8 0d 36 06 00    	sbb    %edi,0x6360d(%eax)
  4007e9:	00 62 72             	add    %ah,0x72(%edx)
int main(int argc, char *argv[] ) 
{
    int i,j,k;
    double tstart, tstop, ttime;
	double gflops = 0.0;
   	float a=1.1;
  4007ec:	79 08                	jns    4007f6 <main+0x196>
  4007ee:	18 05 58 06 00 00    	sbb    %al,0x658
    // scale 1st array and add in the 2nd array
    // example usage - y = mx + b; 
    //
        for(k=0; k<LOOP_COUNT; k++)  
	    {
            fa[k] = a * fa[k] + fb[k];
  4007f4:	62                   	(bad)  
  4007f5:	e1 78                	loope  40086f <main+0x20f>
  4007f7:	08 28                	or     %ch,(%eax)
  4007f9:	05 c2 0c 60 00       	add    $0x600cc2,%eax
  4007fe:	62                   	(bad)  
  4007ff:	f1                   	icebp  
  400800:	78 08                	js     40080a <main+0x1aa>
  400802:	28 3d b8 0c 20 00    	sub    %bh,0x200cb8
  400808:	62 71 78             	bound  %esi,0x78(%ecx)
  40080b:	08 28                	or     %ch,(%eax)
  40080d:	3d ee 0c 60 00       	cmp    $0x600cee,%eax
  400812:	62                   	(bad)  
  400813:	f1                   	icebp  
  400814:	78 08                	js     40081e <main+0x1be>
  400816:	28 35 e4 0c 20 00    	sub    %dh,0x200ce4
  40081c:	62 71 78             	bound  %esi,0x78(%ecx)
  40081f:	08 28                	or     %ch,(%eax)
  400821:	35 1a 0d 60 00       	xor    $0x600d1a,%eax
  400826:	62                   	(bad)  
  400827:	f1                   	icebp  
  400828:	78 08                	js     400832 <main+0x1d2>
  40082a:	28 2d 10 0d 20 00    	sub    %ch,0x200d10
  400830:	62 71 78             	bound  %esi,0x78(%ecx)
  400833:	08 28                	or     %ch,(%eax)
  400835:	2d 46 0d 60 00       	sub    $0x600d46,%eax
  40083a:	62                   	(bad)  
  40083b:	f1                   	icebp  
  40083c:	78 08                	js     400846 <main+0x1e6>
  40083e:	28 25 3c 0d 20 00    	sub    %ah,0x200d3c
  400844:	62 71 78             	bound  %esi,0x78(%ecx)
  400847:	08 28                	or     %ch,(%eax)
  400849:	25 72 0d 60 00       	and    $0x600d72,%eax
  40084e:	62                   	(bad)  
  40084f:	f1                   	icebp  
  400850:	78 08                	js     40085a <main+0x1fa>
  400852:	28 1d 68 0d 20 00    	sub    %bl,0x200d68
  400858:	62 71 78             	bound  %esi,0x78(%ecx)
  40085b:	08 28                	or     %ch,(%eax)
  40085d:	1d 9e 0d 60 00       	sbb    $0x600d9e,%eax
  400862:	62                   	(bad)  
  400863:	f1                   	icebp  
  400864:	78 08                	js     40086e <main+0x20e>
  400866:	28 15 94 0d 20 00    	sub    %dl,0x200d94
  40086c:	62 71 78             	bound  %esi,0x78(%ecx)
  40086f:	08 28                	or     %ch,(%eax)
  400871:	15 ca 0d 60 00       	adc    $0x600dca,%eax
  400876:	62                   	(bad)  
  400877:	f1                   	icebp  
  400878:	78 08                	js     400882 <main+0x222>
  40087a:	28 0d c0 0d 20 00    	sub    %cl,0x200dc0
  400880:	62 71 78             	bound  %esi,0x78(%ecx)
  400883:	08 28                	or     %ch,(%eax)
  400885:	0d f6 0d 60 00       	or     $0x600df6,%eax
  40088a:	62                   	(bad)  
  40088b:	f1                   	icebp  
  40088c:	78 08                	js     400896 <main+0x236>
  40088e:	28 05 ec 0d 20 00    	sub    %al,0x200dec
  400894:	0f 1f 44 00 00       	nopl   0x0(%eax,%eax,1)
  400899:	0f 1f 80 00 00 00 00 	nopl   0x0(%eax)
  4008a0:	62                   	(bad)  
  4008a1:	e2 39                	loop   4008dc <main+0x27c>
  4008a3:	08 a8 c7 ff c0 62    	or     %ch,0x62c0ffc7(%eax)
  4008a9:	72 39                	jb     4008e4 <main+0x284>
  4008ab:	08 a8 fe 3d 80 f0    	or     %ch,-0xf7fc202(%eax)

    printf("Starting Compute\r\n");

    tstart = dtime();	
    // loop many times to really get lots of calculations
    for(j=0; j<MAXFLOPS_ITERS; j++)  
  4008b1:	fa                   	cli    
  4008b2:	02 62 72             	add    0x72(%edx),%ah
    // scale 1st array and add in the 2nd array
    // example usage - y = mx + b; 
    //
        for(k=0; k<LOOP_COUNT; k++)  
	    {
            fa[k] = a * fa[k] + fb[k];
  4008b5:	39 08                	cmp    %ecx,(%eax)
  4008b7:	a8 f5                	test   $0xf5,%al
  4008b9:	62 72 39             	bound  %esi,0x39(%edx)
  4008bc:	08 a8 ec 62 72 39    	or     %ch,0x397262ec(%eax)
  4008c2:	08 a8 e3 62 72 39    	or     %ch,0x397262e3(%eax)
  4008c8:	08 a8 da 62 72 39    	or     %ch,0x397262da(%eax)
  4008ce:	08 a8 d1 62 72 39    	or     %ch,0x397262d1(%eax)
  4008d4:	08 a8 c8 62 e2 39    	or     %ch,0x39e262c8(%eax)
  4008da:	08 a8 c7 62 72 39    	or     %ch,0x397262c7(%eax)
  4008e0:	08 a8 fe 62 72 39    	or     %ch,0x397262fe(%eax)
  4008e6:	08 a8 f5 62 72 39    	or     %ch,0x397262f5(%eax)
  4008ec:	08 a8 ec 62 72 39    	or     %ch,0x397262ec(%eax)
  4008f2:	08 a8 e3 62 72 39    	or     %ch,0x397262e3(%eax)
  4008f8:	08 a8 da 62 72 39    	or     %ch,0x397262da(%eax)
  4008fe:	08 a8 d1 62 72 39    	or     %ch,0x397262d1(%eax)
  400904:	08 a8 c8 72 97 6a    	or     %ch,0x6a9772c8(%eax)
//
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
  40090a:	01 58 48             	add    %ebx,0x48(%eax)
  40090d:	8d 7c 24 08          	lea    0x8(%esp),%edi
  400911:	33 f6                	xor    %esi,%esi
  400913:	c5 f8 92             	(bad)  
  400916:	c8 62 71 78          	enter  $0x7162,$0x78
    // scale 1st array and add in the 2nd array
    // example usage - y = mx + b; 
    //
        for(k=0; k<LOOP_COUNT; k++)  
	    {
            fa[k] = a * fa[k] + fb[k];
  40091a:	08 29                	or     %ch,(%ecx)
  40091c:	0d 5f 0d 60 00       	or     $0x600d5f,%eax
  400921:	62 71 78             	bound  %esi,0x78(%ecx)
  400924:	08 29                	or     %ch,(%ecx)
  400926:	15 15 0d 60 00       	adc    $0x600d15,%eax
  40092b:	62 71 78             	bound  %esi,0x78(%ecx)
  40092e:	08 29                	or     %ch,(%ecx)
  400930:	1d cb 0c 60 00       	sbb    $0x600ccb,%eax
  400935:	62 71 78             	bound  %esi,0x78(%ecx)
  400938:	08 29                	or     %ch,(%ecx)
  40093a:	25 81 0c 60 00       	and    $0x600c81,%eax
  40093f:	62 71 78             	bound  %esi,0x78(%ecx)
  400942:	08 29                	or     %ch,(%ecx)
  400944:	2d 37 0c 60 00       	sub    $0x600c37,%eax
  400949:	62 71 78             	bound  %esi,0x78(%ecx)
  40094c:	08 29                	or     %ch,(%ecx)
  40094e:	35 ed 0b 60 00       	xor    $0x600bed,%eax
  400953:	62 71 78             	bound  %esi,0x78(%ecx)
  400956:	08 29                	or     %ch,(%ecx)
  400958:	3d a3 0b 60 00       	cmp    $0x600ba3,%eax
  40095d:	62                   	(bad)  
  40095e:	e1 78                	loope  4009d8 <main+0x378>
  400960:	08 29                	or     %ch,(%ecx)
  400962:	05 59 0b 60 00       	add    $0x600b59,%eax
//
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
  400967:	62                   	(bad)  
  400968:	e2 f9                	loop   400963 <main+0x303>
  40096a:	09 d1                	or     %edx,%ecx
  40096c:	0c 24                	or     $0x24,%al
  40096e:	e8 1d fc ff ff       	call   400590 <_init+0x80>
     ttime = tstop - tstart;

     //
     // Print the results
     //
     if ((ttime) > 0.0)
  400973:	62                   	(bad)  
  400974:	e2 f9                	loop   40096f <main+0x30f>
  400976:	08 19                	or     %bl,(%ecx)
  400978:	0c 24                	or     $0x24,%al
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
  40097a:	6a 01                	push   $0x1
  40097c:	5a                   	pop    %edx
     ttime = tstop - tstart;

     //
     // Print the results
     //
     if ((ttime) > 0.0)
  40097d:	62                   	(bad)  
  40097e:	f1                   	icebp  
  40097f:	d9 08                	(bad)  (%eax)
  400981:	ef                   	out    %eax,(%dx)
  400982:	e4 c5                	in     $0xc5,%al
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
  400984:	f8                   	clc    
  400985:	92                   	xchg   %eax,%edx
  400986:	ca 62 f1             	lret   $0xf162
  400989:	7a 19                	jp     4009a4 <main+0x344>
  40098b:	7a 44                	jp     4009d1 <main+0x371>
  40098d:	24 04                	and    $0x4,%al
  40098f:	62                   	(bad)  
  400990:	f1                   	icebp  
  400991:	7a 19                	jp     4009ac <main+0x34c>
  400993:	e6 54                	out    %al,$0x54
  400995:	24 05                	and    $0x5,%al
  400997:	62                   	(bad)  
  400998:	f1                   	icebp  
  400999:	7a 19                	jp     4009b4 <main+0x354>
  40099b:	7a 4c                	jp     4009e9 <main+0x389>
  40099d:	24 02                	and    $0x2,%al
  40099f:	62                   	(bad)  
  4009a0:	f1                   	icebp  
  4009a1:	7a 19                	jp     4009bc <main+0x35c>
  4009a3:	e6 5c                	out    %al,$0x5c
  4009a5:	24 03                	and    $0x3,%al
  4009a7:	62                   	(bad)  
  4009a8:	f2 f9                	repnz stc 
  4009aa:	19 98 15 7f 04 00    	sbb    %ebx,0x47f15(%eax)
  4009b0:	00 62 f2             	add    %ah,-0xe(%edx)
  4009b3:	f1                   	icebp  
  4009b4:	19 98 1d 75 04 00    	sbb    %ebx,0x4751d(%eax)
  4009ba:	00 62 f2             	add    %ah,-0xe(%edx)

     // # of gigaflops we just calculated  
     gflops = (double)( 1.0e-9*LOOP_COUNT*MAXFLOPS_ITERS*FLOPSPERCALC);    

 // elasped time
     ttime = tstop - tstart;
  4009bd:	e9 19 b8 1d 5b       	jmp    5b5dc1db <_end+0x5a7dad1b>
  4009c2:	04 00                	add    $0x0,%al
  4009c4:	00 62 b1             	add    %ah,-0x4f(%edx)
  4009c7:	e1 09                	loope  4009d2 <main+0x372>
  4009c9:	5c                   	pop    %esp
  4009ca:	c9                   	leave  

     //
     // Print the results
     //
     if ((ttime) > 0.0)
  4009cb:	62                   	(bad)  
  4009cc:	f1                   	icebp  
  4009cd:	d9 09                	(bad)  (%ecx)
  4009cf:	c2 c1 01             	ret    $0x1c1
  4009d2:	90                   	nop
  4009d3:	c5 f8 98             	(bad)  
  4009d6:	c0 0f 84             	rorb   $0x84,(%edi)
  4009d9:	23 01                	and    (%ecx),%eax
  4009db:	00 00                	add    %al,(%eax)
        }
    }
     tstop = dtime();

     // # of gigaflops we just calculated  
     gflops = (double)( 1.0e-9*LOOP_COUNT*MAXFLOPS_ITERS*FLOPSPERCALC);    
  4009dd:	62                   	(bad)  
  4009de:	f2 f9                	repnz stc 
  4009e0:	08 19                	or     %bl,(%ecx)
  4009e2:	05 41 04 00 00       	add    $0x441,%eax
     //
     // Print the results
     //
     if ((ttime) > 0.0)
     {
         printf("GFlops = %10.3lf, Secs = %10.3lf, GFlops per sec = %10.3lf\r\n",
  4009e7:	62                   	(bad)  
  4009e8:	e1 f9                	loope  4009e3 <main+0x383>
  4009ea:	08 28                	or     %ch,(%eax)
  4009ec:	f9                   	stc    
  4009ed:	6a 01                	push   $0x1
  4009ef:	58                   	pop    %eax
  4009f0:	62 72 79             	bound  %esi,0x79(%edx)
  4009f3:	08 5a 2d             	or     %bl,0x2d(%edx)
  4009f6:	06                   	push   %es
  4009f7:	04 00                	add    $0x0,%al
  4009f9:	00 62 e2             	add    %ah,-0x1e(%edx)
  4009fc:	f9                   	stc    
  4009fd:	08 5b 1d             	or     %bl,0x1d(%ebx)
  400a00:	bc 03 00 00 62       	mov    $0x62000003,%esp
  400a05:	f1                   	icebp  
  400a06:	d9 08                	(bad)  (%eax)
  400a08:	ef                   	out    %eax,(%dx)
  400a09:	e4 c5                	in     $0xc5,%al
  400a0b:	f8                   	clc    
  400a0c:	92                   	xchg   %eax,%edx
  400a0d:	d8 62 32             	fsubs  0x32(%edx)
  400a10:	f9                   	stc    
  400a11:	0b 42 d7             	or     -0x29(%edx),%eax
  400a14:	bf 80 0e 40 00       	mov    $0x400e80,%edi
  400a19:	62 72 f9             	bound  %esi,-0x7(%edx)
  400a1c:	0b 42 c8             	or     -0x38(%edx),%eax
  400a1f:	6a 03                	push   $0x3
  400a21:	58                   	pop    %eax
  400a22:	62 33                	bound  %esi,(%ebx)
  400a24:	f9                   	stc    
  400a25:	0b 26                	or     (%esi),%esp
  400a27:	c7 00 62 51 b1 0b    	movl   $0xbb15162,(%eax)
  400a2d:	5c                   	pop    %esp
  400a2e:	da 62 52             	fisubl 0x52(%edx)
  400a31:	21 48 39             	and    %ecx,0x39(%eax)
  400a34:	e5 62                	in     $0x62,%eax
  400a36:	d1 f9                	sar    %ecx
  400a38:	fb                   	sti    
  400a39:	5a                   	pop    %edx
  400a3a:	e8 62 52 19 58       	call   58595ca1 <_end+0x577947e1>
  400a3f:	3b f5                	cmp    %ebp,%esi
  400a41:	62                   	(bad)  
  400a42:	f2 79 0b             	repnz jns 400a50 <main+0x3f0>
  400a45:	ca f5 62             	lret   $0x62f5
  400a48:	31 89 6b 58 f3 62    	xor    %ecx,0x62f3586b(%ecx)
  400a4e:	f2 79 08             	repnz jns 400a59 <main+0x3f9>
  400a51:	58                   	pop    %eax
  400a52:	1d f9 03 00 00       	sbb    $0x3f9,%eax
  400a57:	62                   	(bad)  
  400a58:	f2 c1 03 55          	repnz roll $0x55,(%ebx)
  400a5c:	e3 62                	jecxz  400ac0 <main+0x460>
  400a5e:	e1 78                	loope  400ad8 <main+0x478>
  400a60:	0b 5a ee             	or     -0x12(%edx),%ebx
  400a63:	62 b1 f9 58 28 fb    	bound  %esi,-0x4d7a707(%ecx)
  400a69:	62                   	(bad)  
  400a6a:	d1 79 00             	sarl   0x0(%ecx)
  400a6d:	72 f6                	jb     400a65 <main+0x405>
  400a6f:	14 62                	adc    $0x62,%al
  400a71:	72 f9                	jb     400a6c <main+0x40c>
  400a73:	08 5b 3d             	or     %bl,0x3d(%ebx)
  400a76:	66 03 00             	add    (%eax),%ax
  400a79:	00 62 f2             	add    %ah,-0xe(%edx)
  400a7c:	f9                   	stc    
  400a7d:	0b 55 e3             	or     -0x1d(%ebp),%edx
  400a80:	62 72 d1             	bound  %esi,-0x2f(%edx)
  400a83:	c3                   	ret    
  400a84:	ac                   	lods   %ds:(%esi),%al
  400a85:	c7                   	(bad)  
  400a86:	62 b1 39 00 72 e0    	bound  %esi,-0x1f8dffc7(%ecx)
  400a8c:	01 62 f1             	add    %esp,-0xf(%edx)
  400a8f:	d9 0b                	(bad)  (%ebx)
  400a91:	c2 d4 00             	ret    $0xd4
  400a94:	62 52 b9             	bound  %edx,-0x47(%edx)
  400a97:	cb                   	lret   
  400a98:	b8 c0 62 41 b9       	mov    $0xb94162c0,%eax
  400a9d:	43                   	inc    %ebx
  400a9e:	db c7                	fcmovnb %st(7),%st
  400aa0:	c5 f8 90             	(bad)  
  400aa3:	ca 62 e2             	lret   $0xe262
  400aa6:	79 08                	jns    400ab0 <main+0x450>
  400aa8:	58                   	pop    %eax
  400aa9:	35 a6 03 00 00       	xor    $0x3a6,%eax
  400aae:	62 a2 b9 cb b8 ed    	bound  %esp,-0x12473447(%edx)
  400ab4:	c5 f8 42             	(bad)  
  400ab7:	cb                   	lret   
  400ab8:	62                   	(bad)  
  400ab9:	e3 f9                	jecxz  400ab4 <main+0x454>
  400abb:	0b 26                	or     (%esi),%esp
  400abd:	e0 00                	loopne 400abf <main+0x45f>
  400abf:	62 81 79 00 fa c8    	bound  %eax,-0x3705ff87(%ecx)
  400ac5:	62 a1 f9 58 6f d3    	bound  %esp,-0x2c90a707(%ecx)
  400acb:	62 a2 c1 03 55 fe    	bound  %esp,-0x1aafc3f(%edx)
  400ad1:	62 21                	bound  %esp,(%ecx)
  400ad3:	d9 c2                	fld    %st(2)
  400ad5:	59                   	pop    %ecx
  400ad6:	dd 62 21             	frstor 0x21(%edx)
  400ad9:	71 00                	jno    400adb <main+0x47b>
  400adb:	fe                   	(bad)  
  400adc:	d2 62 21             	shlb   %cl,0x21(%edx)
  400adf:	f9                   	stc    
  400ae0:	c9                   	leave  
  400ae1:	59                   	pop    %ecx
  400ae2:	df 62 21             	fbld   0x21(%edx)
  400ae5:	a9 43 db d3 62       	test   $0x62d3db43,%eax
  400aea:	01 21                	add    %esp,(%ecx)
  400aec:	00 fe                	add    %bh,%dh
  400aee:	c8 62 01 b1          	enter  $0x162,$0xb1
  400af2:	82                   	(bad)  
  400af3:	59                   	pop    %ecx
  400af4:	da 62 91             	fisubl -0x6f(%edx)
  400af7:	f9                   	stc    
  400af8:	0b 28                	or     (%eax),%ebp
  400afa:	d3 e8                	shr    %cl,%eax
  400afc:	50                   	push   %eax
  400afd:	fa                   	cli    
  400afe:	ff                   	(bad)  
  400aff:	ff                   	(bad)  
                 gflops, ttime, gflops/ttime);
     }	 
     return( 0 );
  400b00:	bf 38 14 60 00       	mov    $0x601438,%edi
  400b05:	33 c0                	xor    %eax,%eax
  400b07:	e8 54 fa ff ff       	call   400560 <_init+0x50>
  400b0c:	33 c0                	xor    %eax,%eax
  400b0e:	48                   	dec    %eax
  400b0f:	89 ec                	mov    %ebp,%esp
  400b11:	5d                   	pop    %ebp
  400b12:	c3                   	ret    
  400b13:	0f 1f 44 00 00       	nopl   0x0(%eax,%eax,1)
  400b18:	0f 1f 84 00 00 00 00 	nopl   0x0(%eax,%eax,1)
  400b1f:	00 

0000000000400b20 <dtime>:
//
// utility routine to return 
// the current wall clock time
//
double dtime()
{
  400b20:	48                   	dec    %eax
  400b21:	83 ec 18             	sub    $0x18,%esp
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
  400b24:	33 f6                	xor    %esi,%esi
  400b26:	48                   	dec    %eax
  400b27:	8d 3c 24             	lea    (%esp),%edi
  400b2a:	e8 61 fa ff ff       	call   400590 <_init+0x80>
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
  400b2f:	ba 01 00 00 00       	mov    $0x1,%edx
  400b34:	c5 f8 92             	(bad)  
  400b37:	ca 62 f1             	lret   $0xf162
  400b3a:	7a 19                	jp     400b55 <dtime+0x35>
  400b3c:	7a 4c                	jp     400b8a <__intel_new_proc_init+0xa>
  400b3e:	24 02                	and    $0x2,%al
  400b40:	62                   	(bad)  
  400b41:	f1                   	icebp  
  400b42:	7a 19                	jp     400b5d <dtime+0x3d>
  400b44:	e6 44                	out    %al,$0x44
  400b46:	24 03                	and    $0x3,%al
  400b48:	62                   	(bad)  
  400b49:	f1                   	icebp  
  400b4a:	7a 19                	jp     400b65 <dtime+0x45>
  400b4c:	7a 14                	jp     400b62 <dtime+0x42>
  400b4e:	24 62                	and    $0x62,%al
  400b50:	f1                   	icebp  
  400b51:	7a 19                	jp     400b6c <dtime+0x4c>
  400b53:	e6 5c                	out    %al,$0x5c
  400b55:	24 01                	and    $0x1,%al
  400b57:	62                   	(bad)  
  400b58:	f2 f1                	repnz icebp 
  400b5a:	19 98 05 df 02 00    	sbb    %ebx,0x2df05(%eax)
  400b60:	00 62 f2             	add    %ah,-0xe(%edx)
  400b63:	e9 19 98 1d d5       	jmp    d55da381 <_end+0xd47d8ec1>
  400b68:	02 00                	add    (%eax),%al
  400b6a:	00 62 f2             	add    %ah,-0xe(%edx)
    return( tseconds );
  400b6d:	e1 19                	loope  400b88 <__intel_new_proc_init+0x8>
  400b6f:	98                   	cwtl   
  400b70:	05 c3 02 00 00       	add    $0x2c3,%eax
  400b75:	48                   	dec    %eax
  400b76:	83 c4 18             	add    $0x18,%esp
  400b79:	c3                   	ret    
  400b7a:	66 0f 1f 44 00 00    	nopw   0x0(%eax,%eax,1)

0000000000400b80 <__intel_new_proc_init>:
  400b80:	c3                   	ret    
  400b81:	0f 1f 84 00 00 00 00 	nopl   0x0(%eax,%eax,1)
  400b88:	00 
  400b89:	0f 1f 80 00 00 00 00 	nopl   0x0(%eax)

0000000000400b90 <__intel_new_proc_init_R>:
  400b90:	56                   	push   %esi
  400b91:	89 fe                	mov    %edi,%esi
  400b93:	bf 03 00 00 00       	mov    $0x3,%edi
  400b98:	48                   	dec    %eax
  400b99:	83 c4 08             	add    $0x8,%esp
  400b9c:	e9 0f 00 00 00       	jmp    400bb0 <__intel_proc_init_ftzdazule>
  400ba1:	0f 1f 84 00 00 00 00 	nopl   0x0(%eax,%eax,1)
  400ba8:	00 
  400ba9:	0f 1f 80 00 00 00 00 	nopl   0x0(%eax)

0000000000400bb0 <__intel_proc_init_ftzdazule>:
  400bb0:	41                   	inc    %ecx
  400bb1:	54                   	push   %esp
  400bb2:	53                   	push   %ebx
  400bb3:	55                   	push   %ebp
  400bb4:	48                   	dec    %eax
  400bb5:	81 ec 00 02 00 00    	sub    $0x200,%esp
  400bbb:	89 f3                	mov    %esi,%ebx
  400bbd:	89 dd                	mov    %ebx,%ebp
  400bbf:	41                   	inc    %ecx
  400bc0:	89 dc                	mov    %ebx,%esp
  400bc2:	83 e5 02             	and    $0x2,%ebp
  400bc5:	41                   	inc    %ecx
  400bc6:	83 e4 04             	and    $0x4,%esp
  400bc9:	85 ed                	test   %ebp,%ebp
  400bcb:	74 09                	je     400bd6 <__intel_proc_init_ftzdazule+0x26>
  400bcd:	b8 02 00 00 00       	mov    $0x2,%eax
  400bd2:	85 f8                	test   %edi,%eax
  400bd4:	74 0a                	je     400be0 <__intel_proc_init_ftzdazule+0x30>
  400bd6:	45                   	inc    %ebp
  400bd7:	85 e4                	test   %esp,%esp
  400bd9:	74 35                	je     400c10 <__intel_proc_init_ftzdazule+0x60>
  400bdb:	83 e7 04             	and    $0x4,%edi
  400bde:	75 30                	jne    400c10 <__intel_proc_init_ftzdazule+0x60>
  400be0:	48                   	dec    %eax
  400be1:	8d 3c 24             	lea    (%esp),%edi
  400be4:	33 f6                	xor    %esi,%esi
  400be6:	ba 00 02 00 00       	mov    $0x200,%edx
  400beb:	e8 90 f9 ff ff       	call   400580 <_init+0x70>
  400bf0:	0f ae 04 24          	fxsave (%esp)
  400bf4:	f6 44 24 1c 40       	testb  $0x40,0x1c(%esp)
  400bf9:	75 05                	jne    400c00 <__intel_proc_init_ftzdazule+0x50>
  400bfb:	bd 00 00 00 00       	mov    $0x0,%ebp
  400c00:	f7 44 24 1c 00 00 02 	testl  $0x20000,0x1c(%esp)
  400c07:	00 
  400c08:	75 06                	jne    400c10 <__intel_proc_init_ftzdazule+0x60>
  400c0a:	41                   	inc    %ecx
  400c0b:	bc 00 00 00 00       	mov    $0x0,%esp
  400c10:	83 e3 01             	and    $0x1,%ebx
  400c13:	75 39                	jne    400c4e <__intel_proc_init_ftzdazule+0x9e>
  400c15:	85 ed                	test   %ebp,%ebp
  400c17:	74 11                	je     400c2a <__intel_proc_init_ftzdazule+0x7a>
  400c19:	0f ae 1c 24          	stmxcsr (%esp)
  400c1d:	8b 04 24             	mov    (%esp),%eax
  400c20:	83 c8 40             	or     $0x40,%eax
  400c23:	89 04 24             	mov    %eax,(%esp)
  400c26:	0f ae 14 24          	ldmxcsr (%esp)
  400c2a:	45                   	inc    %ebp
  400c2b:	85 e4                	test   %esp,%esp
  400c2d:	74 13                	je     400c42 <__intel_proc_init_ftzdazule+0x92>
  400c2f:	0f ae 1c 24          	stmxcsr (%esp)
  400c33:	8b 04 24             	mov    (%esp),%eax
  400c36:	0d 00 00 02 00       	or     $0x20000,%eax
  400c3b:	89 04 24             	mov    %eax,(%esp)
  400c3e:	0f ae 14 24          	ldmxcsr (%esp)
  400c42:	48                   	dec    %eax
  400c43:	81 c4 00 02 00 00    	add    $0x200,%esp
  400c49:	5d                   	pop    %ebp
  400c4a:	5b                   	pop    %ebx
  400c4b:	41                   	inc    %ecx
  400c4c:	5c                   	pop    %esp
  400c4d:	c3                   	ret    
  400c4e:	0f ae 1c 24          	stmxcsr (%esp)
  400c52:	8b 04 24             	mov    (%esp),%eax
  400c55:	0d 00 80 00 00       	or     $0x8000,%eax
  400c5a:	89 04 24             	mov    %eax,(%esp)
  400c5d:	0f ae 14 24          	ldmxcsr (%esp)
  400c61:	eb b2                	jmp    400c15 <__intel_proc_init_ftzdazule+0x65>
  400c63:	0f 1f 44 00 00       	nopl   0x0(%eax,%eax,1)
  400c68:	0f 1f 84 00 00 00 00 	nopl   0x0(%eax,%eax,1)
  400c6f:	00 

0000000000400c70 <__libc_csu_init>:
  400c70:	41                   	inc    %ecx
  400c71:	57                   	push   %edi
  400c72:	4c                   	dec    %esp
  400c73:	8d 3d 23 05 20 00    	lea    0x200523,%edi
  400c79:	41                   	inc    %ecx
  400c7a:	56                   	push   %esi
  400c7b:	49                   	dec    %ecx
  400c7c:	89 d6                	mov    %edx,%esi
  400c7e:	41                   	inc    %ecx
  400c7f:	55                   	push   %ebp
  400c80:	49                   	dec    %ecx
  400c81:	89 f5                	mov    %esi,%ebp
  400c83:	41                   	inc    %ecx
  400c84:	54                   	push   %esp
  400c85:	41                   	inc    %ecx
  400c86:	89 fc                	mov    %edi,%esp
  400c88:	55                   	push   %ebp
  400c89:	48                   	dec    %eax
  400c8a:	8d 2d 0c 05 20 00    	lea    0x20050c,%ebp
  400c90:	53                   	push   %ebx
  400c91:	49                   	dec    %ecx
  400c92:	29 ef                	sub    %ebp,%edi
  400c94:	51                   	push   %ecx
  400c95:	e8 76 f8 ff ff       	call   400510 <_init>
  400c9a:	49                   	dec    %ecx
  400c9b:	c1 ff 03             	sar    $0x3,%edi
  400c9e:	74 1d                	je     400cbd <__libc_csu_init+0x4d>
  400ca0:	31 db                	xor    %ebx,%ebx
  400ca2:	66 0f 1f 44 00 00    	nopw   0x0(%eax,%eax,1)
  400ca8:	4c                   	dec    %esp
  400ca9:	89 f2                	mov    %esi,%edx
  400cab:	4c                   	dec    %esp
  400cac:	89 ee                	mov    %ebp,%esi
  400cae:	44                   	inc    %esp
  400caf:	89 e7                	mov    %esp,%edi
  400cb1:	ff 54 dd 00          	call   *0x0(%ebp,%ebx,8)
  400cb5:	48                   	dec    %eax
  400cb6:	ff c3                	inc    %ebx
  400cb8:	4c                   	dec    %esp
  400cb9:	39 fb                	cmp    %edi,%ebx
  400cbb:	75 eb                	jne    400ca8 <__libc_csu_init+0x38>
  400cbd:	58                   	pop    %eax
  400cbe:	5b                   	pop    %ebx
  400cbf:	5d                   	pop    %ebp
  400cc0:	41                   	inc    %ecx
  400cc1:	5c                   	pop    %esp
  400cc2:	41                   	inc    %ecx
  400cc3:	5d                   	pop    %ebp
  400cc4:	41                   	inc    %ecx
  400cc5:	5e                   	pop    %esi
  400cc6:	41                   	inc    %ecx
  400cc7:	5f                   	pop    %edi
  400cc8:	c3                   	ret    
  400cc9:	0f 1f 80 00 00 00 00 	nopl   0x0(%eax)

0000000000400cd0 <__libc_csu_fini>:
  400cd0:	c3                   	ret    
  400cd1:	66 90                	xchg   %ax,%ax
  400cd3:	90                   	nop

0000000000400cd4 <__do_global_ctors_aux>:
  400cd4:	55                   	push   %ebp
  400cd5:	48                   	dec    %eax
  400cd6:	89 e5                	mov    %esp,%ebp
  400cd8:	53                   	push   %ebx
  400cd9:	bb a0 11 60 00       	mov    $0x6011a0,%ebx
  400cde:	52                   	push   %edx
  400cdf:	eb 06                	jmp    400ce7 <__do_global_ctors_aux+0x13>
  400ce1:	48                   	dec    %eax
  400ce2:	83 eb 08             	sub    $0x8,%ebx
  400ce5:	ff d0                	call   *%eax
  400ce7:	48                   	dec    %eax
  400ce8:	8b 03                	mov    (%ebx),%eax
  400cea:	48                   	dec    %eax
  400ceb:	83 f8 ff             	cmp    $0xffffffff,%eax
  400cee:	75 f1                	jne    400ce1 <__do_global_ctors_aux+0xd>
  400cf0:	58                   	pop    %eax
  400cf1:	5b                   	pop    %ebx
  400cf2:	5d                   	pop    %ebp
  400cf3:	c3                   	ret    

Disassembly of section .fini:

0000000000400cf4 <_fini>:
  400cf4:	48                   	dec    %eax
  400cf5:	83 ec 08             	sub    $0x8,%esp
  400cf8:	e8 e7 f8 ff ff       	call   4005e4 <__do_global_dtors_aux>
  400cfd:	48                   	dec    %eax
  400cfe:	83 c4 08             	add    $0x8,%esp
  400d01:	c3                   	ret    
