

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "time.h"

#include <cstdlib>
#include <iostream>

#define NUM_BLOCKS 8192*2
#define NUM_THREADS_PER_BLOCK 256
#define N NUM_BLOCKS*NUM_THREADS_PER_BLOCK
#define RADIUS 1000



using namespace std;

void checkCUDAError (const char *msg) {
	cudaError_t err = cudaGetLastError();
	if( cudaSuccess != err) {
		cerr << "Cuda error: " << msg << ", " << cudaGetErrorString( err) << endl;
		exit(-1);
	}
}


void initArray(float* A, int nElems) {
	srand(time(NULL));
	for( int i=0; i<nElems; i++)
		A[i] = (float)(rand() % (10));
}


void print_arrayfloat (float A[N]) {
    int i;
    
    for (i = 0; i < N; i++)
            printf("%2.0f ", A[i]);
    printf("\n");
}


int compare(float *A,float *B) {
    int i;
    
    for (i = 0; i < N; i++)
    	if(A[i]!=B[i])
			return 0;
    return 1;
}



void stencil1D(int start, int end, float *in, float *out)
{
	for (int i = start; i < end; i++) {
		out[i] = 0.0f;//printf("\ni=%d-",i);
		for (int j = -RADIUS; j <= RADIUS; j++){
			if (i+j<end && i+j>=start){
				out[i] += in[j+i];
				//printf("%d ",j+i);
			}
		}
	}
}

__global__ void stencil1D_CUDA(int start, int end, float *in, float *out)
{
	int i = start + blockIdx.x * blockDim.x + threadIdx.x;
		//out[i] = 0.0f;
		for (int j = -RADIUS; j <= RADIUS; j++){
			if (i+j<end && i+j>=start){
				out[i] += in[j+i];
			}
		}
}


void stencil1D_offset(int start, int end, int offset, float *in, float *out)
{
	int tam = end-start,x=0;
	for( int i = offset;x<end;x++){
		out[i] = 0.0f;//printf("\ni=%d-",i);
		for (int j = -RADIUS; j <= RADIUS; j++){
			if (i+j<end && i+j>=start){
				out[i] += in[j+i];
			}
		}
		i = (i+1)%(tam);
	}
}


__global__ void stencil1D_offset_CUDA(int start, int end, int offset, float *in, float *out)
{
	int i = (start + blockIdx.x * blockDim.x + threadIdx.x + offset)%(end-start);
	out[i] = 0.0f;//printf("\ni=%d-",i);
	for (int j = -RADIUS; j <= RADIUS; j++){
		if (i+j<end && i+j>=start){
			out[i] += in[j+i];
		}
	}
}



void stencil1D_stride(int start, int end, int stride, float *in, float *out)
{	
	int tam = end-start,x=0,ic,strs;
	for( int i = 0;x<end;x++){
		
		ic=i;
		strs=0;
		ic = (i*(stride+1));
		for(;ic>=tam;){
			ic -= tam;
			strs++;
		}
		//printf("%d %d %d\n",i,ic,strs);
		if(tam%stride==0)
			ic += strs;
		out[i] = 0.0f;
	     //printf("\ni=%d faz %d",start + blockIdx.x * blockDim.x + threadIdx.x,i);
			
			for (int j = -RADIUS; j <= RADIUS; j++){
				if (i+j<end && i+j>=start){
					out[ic] += in[j+i];
				}
			}
	}
}


__global__ void stencil1D_stride_CUDA(int start, int end, int stride, float *in, float *out)
{	
	int i = start + blockIdx.x * blockDim.x + threadIdx.x;
	int ic=i;
	int strs=0,tam = end-start;
	ic = (i*(stride+1));
	for(;ic>=tam;){
		ic -= tam;
		strs++;
	}
	//printf("%d %d %d\n",i,ic,strs);
	if(tam%stride==0)
		i = ic+strs;
	else
		i=ic;
	out[i] = 0.0f;
     //printf("\ni=%d faz %d",start + blockIdx.x * blockDim.x + threadIdx.x,i);
		
		for (int j = -RADIUS; j <= RADIUS; j++){
			if (i+j<end && i+j>=start){
				out[i] += in[j+i];
			}
		}
}




void quicksort(int first,int last,float * out){
    int pivot,j,i;
    float temp;

     if(first<last){
         pivot=first;
         i=first;
         j=last;

         while(i<j){
             while(out[i]<=out[pivot]&&i<last)
                 i++;
             while(out[j]>out[pivot])
                 j--;
             if(i<j){
                 temp=out[i];
                  out[i]=out[j];
                  out[j]=temp;
             }
         }

         temp=out[pivot];
         out[pivot]=out[j];
         out[j]=temp;
         quicksort(first,j-1,out);
         quicksort(j+1,last,out);

    }
}



int main(int argc, char* argv[]) {

	int size = N * sizeof(float);
	int wsize = (2 * RADIUS + 1) * sizeof(float);
	clock_t begin,end,totalTime;

	int nstreams = 32; // One stream for each pair of kernels
	float kernel_time = 10; // Time each kernel should run in ms
	float elapsed_time;
//	int cuda_device = 0;


	//SEQ
	float *Temp = (float *)malloc(wsize);
	float *A = (float *)malloc(size);
	float *B = (float *)malloc(size);
	
	float *cuda_out= (float *)malloc(size);
	initArray(A, N);

	//CUDA
	float *d_Temp; cudaMalloc(&d_Temp, wsize);
	float *d_A; cudaMalloc(&d_A, size);
	float *d_B; cudaMalloc(&d_B, size);
	checkCUDAError("mem allocation");

    //cudaMemcpy(d_Temp, d_Temp, wsize, cudaMemcpyHostToDevice);
	
	cudaMemcpy(d_A, A, size, cudaMemcpyHostToDevice);
	checkCUDAError("memcpy h->d");

	dim3 dimGrid (NUM_BLOCKS);
	dim3 dimBlock (NUM_THREADS_PER_BLOCK);


	cudaEvent_t start_event, stop_event;
	cudaEventCreate(&start_event);
	cudaEventCreate(&stop_event);
	cudaEventRecord(start_event, 0);

	//stencil1D_CUDA<<<NUM_BLOCKS, dimBlock>>>(0, N, d_A, d_B);
	stencil1D_stride_CUDA<<<NUM_BLOCKS, dimBlock>>>(0, N,3 ,d_A, d_B);

	cudaEventRecord(stop_event, 0);

	cudaEventSynchronize(stop_event);
	cudaEventElapsedTime(&elapsed_time, start_event, stop_event);

	printf("\nExpected time for serial execution of %d sets of kernels is between approx. %.3fs and %.3fs\n", nstreams, (nstreams + 1) * kernel_time / 1000.0f, 2 * nstreams *kernel_time / 1000.0f);
	printf("Expected time for fully concurrent execution of %d sets of kernels is approx. %.3fs\n", nstreams, 2 * kernel_time / 1000.0f);
	printf("Measured time for sample = %.3fs\n", elapsed_time / 1000.0f);

	cudaThreadSynchronize();
	checkCUDAError("kernel invocation");
	cudaMemcpy(cuda_out, d_B, size, cudaMemcpyDeviceToHost);


	checkCUDAError("memcpy d->h");


	begin = clock();
	//stencil1D(0,N,A,B);
	stencil1D_stride(0,N,3,A,B);
	end = clock();
	totalTime = end-begin;
	printf("tempo SEQ= %1.4f \n",(float)((totalTime)/(float)(CLOCKS_PER_SEC)));
  

	/*
	printf("\nA - \n");
	print_arrayfloat(A);
	printf("\nB - \n");
	print_arrayfloat(B);
	printf("\nCUDA - \n");
	print_arrayfloat(cuda_out);*/

	if(compare(B,cuda_out))
		printf("\nIGUAIS\n");
	else
		printf("\nDIFERENTES\n");

	//free
	free(Temp); free(A); free(B);

	cudaFree(d_A);cudaFree(d_B);cudaFree(d_Temp);
	checkCUDAError("Cmem free");

    return 0;
}
